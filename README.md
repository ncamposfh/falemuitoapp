# FaleMuito - Fundação Casper Líbero

Projeto desenvolvido para a Fundação Casper Líbero. Desenvolvido em AngularJS com testes em Karma.

## Observações

  O projeto foi inicializado utilizando FountainJS Generator Angular 1.

* [generator-fountain-angular1](https://github.com/FountainJS/generator-fountain-angular1) - Link com instruções para o gerador.


## Instalação e Dependências

É necessário NodeJS, npm e bower, as dependências podem ser instaladas com os comandos abaixo:


```sh
$ npm install
$ bower install
```

Depois de tudo instalado, pode rodar o servidor com o comando: 

```sh
$ gulp serve
```

A aplicação iniciará em localhost:3000


## HOME.js

O core do projeto com as definições de tarifas, ddds e cálculos. As validações são realizadas com as próprias diretivas do AngularJS.

```sh
src/app/home.js
```


## Licença

Projeto exclusivamente desenvolvido para avaliação da Fundação Casper Líbero.