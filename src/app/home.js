angular
  .module('app')
  .component('home', {
    templateUrl: 'app/home.html',
    controller: function () {
    var $Ctrl = this;

    // Lista DDDs
    $Ctrl.ddds = [
        {name: 'São Paulo', ddd: 11},
        {name: 'Ribeirão Preto', ddd: 16},
        {name: 'Mirassol', ddd: 17},
        {name: 'Tupi Paulista', ddd: 18}
    ];

    // Inicia dados para model
    $Ctrl.conexao = {
        'origem': 0,
        'destino': 0
    };
    $Ctrl.duracao = '';
    $Ctrl.plano = {};

    //Valores das tarifas
    $Ctrl.tarifa = [
        {id: 0, valor: 0.0},
        {id: 1, valor: 0.90},
        {id: 2, valor: 1.70},
        {id: 3, valor: 1.90},
        {id: 4, valor: 2.70},
        {id: 5, valor: 2.90}
    ];

    // Planos
    $Ctrl.planos = [
        {nome: 'FaleMuito 30', tempo: 30, cplano: 0, splano: 0},
        {nome: 'FaleMuito 60', tempo: 60, cplano: 0, splano: 0},
        {nome: 'FaleMuito 120', tempo: 120, cplano: 0, splano: 0}
    ];


    $Ctrl.calculo = function () {

        $Ctrl.erroralert = false;

        if ($Ctrl.conexao.origem === 11 && $Ctrl.conexao.destino === 16) {
            // Valor dos minutos extras com plano:
            $Ctrl.minutosextas($Ctrl.duracao, $Ctrl.tarifa[3].valor);
        }

        else if ($Ctrl.conexao.origem === 16 && $Ctrl.conexao.destino === 11) {
            // Valor dos minutos extras com plano:
            $Ctrl.minutosextas($Ctrl.duracao, $Ctrl.tarifa[5].valor);
        }

        else if ($Ctrl.conexao.origem === 11 && $Ctrl.conexao.destino === 17) {
            // Valor dos minutos extras com plano:
            $Ctrl.minutosextas($Ctrl.duracao, $Ctrl.tarifa[2].valor);
        }

        else if ($Ctrl.conexao.origem === 17 && $Ctrl.conexao.destino === 11) {
            // Valor dos minutos extras com plano:
            $Ctrl.minutosextas($Ctrl.duracao, $Ctrl.tarifa[4].valor);
        }

        else if ($Ctrl.conexao.origem === 11 && $Ctrl.conexao.destino === 18) {
            // Valor dos minutos extras com plano:
            $Ctrl.minutosextas($Ctrl.duracao, $Ctrl.tarifa[1].valor);
        }

        else if ($Ctrl.conexao.origem === 18 && $Ctrl.conexao.destino === 11) {
            // Valor dos minutos extras com plano:
            $Ctrl.minutosextas($Ctrl.duracao, $Ctrl.tarifa[3].valor);
        } else {

            $Ctrl.plano.cplano = 0;
            $Ctrl.plano.splano = 0;

            $Ctrl.erroralert = true;

            $(".alert-warning").fadeTo(4000, 500).slideUp(500, function(){
                $(".alert-warning").slideUp(500);
            })

            $Ctrl.minutosextas(0,0);

        }

        return;

    };

    $Ctrl.minutosextas = function ($tempo , $tarifa) {

        if($tempo > $Ctrl.planos[0].tempo) {
            $Ctrl.planos[0].cplano = ($tempo - $Ctrl.planos[0].tempo) * ($tarifa + ($tarifa *  0.10) );
        } else {
            $Ctrl.planos[0].cplano = 0;
        }

        if($tempo > $Ctrl.planos[1].tempo) {
            $Ctrl.planos[1].cplano = ($tempo - $Ctrl.planos[1].tempo) * ($tarifa + ($tarifa *  0.10) );
        } else {
            $Ctrl.planos[1].cplano = 0;
        }

        if($tempo > $Ctrl.planos[2].tempo) {
            $Ctrl.planos[2].cplano = ($tempo - $Ctrl.planos[2].tempo) * ($tarifa + ($tarifa *  0.10) );
        } else {
            $Ctrl.planos[2].cplano = 0;
        }

        // Valor sem plano
        $Ctrl.planos[0].splano = $tarifa * $Ctrl.duracao;
        $Ctrl.planos[1].splano = $tarifa * $Ctrl.duracao;
        $Ctrl.planos[2].splano = $tarifa * $Ctrl.duracao;



    }






}
});

